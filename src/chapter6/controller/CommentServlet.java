package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Comment;
import chapter6.beans.User;
import chapter6.service.CommentService;


@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	HttpSession session = request.getSession();
    	Comment comment = new Comment();
    	String commentText = request.getParameter("commentText");
    	Integer messageId = Integer.parseInt(request.getParameter("commentMessageId"));
    	User user = (User) session.getAttribute("loginUser");
    	List<String> errorMessages = new ArrayList<String>();

    	if (!isValid(commentText, errorMessages)) {
    		session.setAttribute("errorMessages", errorMessages);
    		response.sendRedirect("./");
    		return;
    	}

    	comment.setText(commentText);
    	comment.setMessageId(messageId);
    	comment.setUserId(user.getId());

    	new CommentService().insert(comment);
    	response.sendRedirect("./");
    }

    private boolean isValid(String commentText, List<String> errorMessages) {
    	if (StringUtils.isBlank(commentText)) {
    		errorMessages.add("メッセージを入力してください");
    	} else if (140 < commentText.length()) {
            errorMessages.add("140文字以下で入力してください");
        }
        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}