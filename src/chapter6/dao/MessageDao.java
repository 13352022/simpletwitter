package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.Message;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.exception.SQLRuntimeException;

public class MessageDao {

    public void insert(Connection connection, Message message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO messages ( ");
            sql.append("    user_id, ");
            sql.append("    text, ");
            sql.append("    created_date, ");
            sql.append("    updated_date ");
            sql.append(") VALUES ( ");
            sql.append("    ?, ");                  // user_id
            sql.append("    ?, ");                  // text
            sql.append("    CURRENT_TIMESTAMP, ");  // created_date
            sql.append("    CURRENT_TIMESTAMP ");   // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, message.getUserId());
            ps.setString(2, message.getText());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public Message select(Connection connection, int id) {
    	PreparedStatement ps = null;

    	try {
    		StringBuilder sql = new StringBuilder();
    		sql.append("SELECT *");
    		sql.append("FROM messages ");
    		sql.append("WHERE id = ? ");

    		ps = connection.prepareStatement(sql.toString());
    		ps.setInt(1, id);

    		ResultSet rs = ps.executeQuery();

    		List<Message> messages = toEditMessages(rs);
    		if(messages.isEmpty()) {
    			return null;
    		} else if (2 <= messages.size()) {
    			throw new IllegalStateException("つぶやきが重複しています");
    		} else {
    			return messages.get(0);
    		}

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void update(Connection connection, Message message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE messages SET ");
            sql.append("text = ? ");
            sql.append("WHERE id = ? ");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, message.getText());
            ps.setInt(2, message.getId());

            ps.executeUpdate();
            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void delete(Connection connection, int id) {

    	PreparedStatement ps = null;

    	try {
    		StringBuilder sql = new StringBuilder();
    		sql.append("DELETE FROM messages ");
    		sql.append("WHERE id = ?");

    		ps = connection.prepareStatement(sql.toString());

    		ps.setInt(1, id);

    		ps.executeUpdate();

    	} catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<Message> toEditMessages(ResultSet rs) throws SQLException {

    	List<Message> editMessages = new ArrayList<Message>();

    	try {
    		while (rs.next()) {
    			Message editMessage = new Message();
    			editMessage.setId(rs.getInt("id"));
    			editMessage.setText(rs.getString("text"));
    			editMessage.setUserId(rs.getInt("user_id"));
    			editMessage.setCreatedDate(rs.getTimestamp("created_date"));
    			editMessage.setUpdatedDate(rs.getTimestamp("updated_date"));

    			editMessages.add(editMessage);
    		}
    		return editMessages;
    	} finally {
    		close(rs);
    	}
    }
}

